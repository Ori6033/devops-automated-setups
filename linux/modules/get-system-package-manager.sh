#!/bin/bash

# Returns the package manager for Debian, Fedora/CentOS, and Arch distros
# @returns {string}

if [ -x "$(command -v apt)" ]
then
  echo "apt"
elif [ -x "$(command -v dnf)" ]
then
  echo "dnf"
elif [ -x "$(command -v pacman)" ]
then
  echo "pacman"
else
  echo "ERROR: Package manager not found."
fi

