#!/bin/bash

# Installation and setup for openvpn

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "openvpn"

