#!/bin/bash

# Installation and setup for Python

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "python"

