#!/bin/bash

# Installation and setup of Docker

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh
PACKAGE_MANAGER=$(~/devops-automated-setups/linux/modules/get-system-package-manager.sh)

# Setup
if [ $PACKAGE_MANAGER == "dnf" ]
then
  sudo $PACKAGE_MANAGER config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
elif [ $PACKAGE_MANAGER == "apt" ]
then
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
fi
$PACKAGE_INSTALLER "docker docker-ce docker-ce-cli containerd.io docker-compose"

# Start docker
sudo systemctl start docker
# Run at boot
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
# Run docker rootless
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R

