#!/bin/bash

# rclone sync borg with cloud provider then clean the remote bucket
# @param {string} borg_repo - Path to borg repository
# @param {string} name_of_storage - rclone storage name
# @param {string} bucket_name - Cloud hosted bucket name
# @example
#   bash rclone-sync-borg.sh /media/sda1/borg-repo "b2" "bucket"

# PARAMS
borg_repo=$1
name_of_storage=$2
bucket_name=$3

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"

$message "syncing borg repo to $name_of_storage:$bucket_name"
rclone sync $borg_repo "$name_of_storage:$bucket_name" -P
$message "done syncing borg repo to $name_of_storage:$bucket_name"

$message "Cleaning up $name_of_storage:$bucket_name"
rclone cleanup "$name_of_storage:$bucket_name"
$message "Done cleaning up $name_of_storage:$bucket_name"
