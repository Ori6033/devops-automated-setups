#!/bin/bash

# Vim setup for a dev environment

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "vim-enhanced"
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
cd ~/.vim/bundle
git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
git clone https://github.com/myusuf3/numbers.vim.git
git clone https://github.com/vim-airline/vim-airline.git
git clone https://github.com/dracula/vim.git dracula
git clone https://github.com/preservim/nerdtree.git
git clone https://github.com/heavenshell/vim-jsdoc.git
git clone https://github.com/kien/ctrlp.vim.git

cd ~
cat > .vimrc <<- "EOF"
execute pathogen#infect()
syntax enable
filetype plugin indent on
" On pressing tab, insert 2 spaces
set expandtab
" show existing tab with 2 spaces width
set tabstop=2
set softtabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
set autoindent

" yank across terminals
if (system'uname -s') == "Darwin\n"
  set clipboard=unnamed "OSX"
else
  set clipboard=unnamedplus "yank across terminals
endif

autocmd BufWritePre * :%s/\s\+$//e " remove whitespaces on save

nnoremap <F3> :NumbersToggle<CR>
nnoremap <F4> :NumbersOnOff<CR>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

:set colorcolumn=100

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

colorscheme dracula
EOF

