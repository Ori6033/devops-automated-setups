#!/bin/bash

# Cronie installation and setup

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
package_installer="$location_dir/package-installer.sh"

# Setup
$package_installer "cronie"
sudo systemctl enable crond.service
sudo systemctl start crond.service

