#!/bin/bash

# Syncthing installation and setup

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
package_installer="$location_dir/package-installer.sh"

$package_installer "syncthing"
sudo systemctl enable syncthing@$USER.service
sudo systemctl start syncthing@$USER.service

