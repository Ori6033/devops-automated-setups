#!/bin/bash

# Installation and setup for wireguard-tools

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "wireguard-tools"

