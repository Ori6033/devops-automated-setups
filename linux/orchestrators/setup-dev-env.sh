#!/bin/bash

# Setup a dev environment for multiple distros

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh
PACKAGE_MANAGER=$(~/devops-automated-setups/linux/modules/get-system-package-manager.sh)
SETUP_VIM=~/devops-automated-setups/linux/modules/setup-vim.sh
SETUP_TMUX=~/devops-automated-setups/linux/modules/setup-tmux.sh
SETUP_POSTGRESQL=~/devops-automated-setups/linux/modules/setup-postgresql.sh
SETUP_CRONIE=~/devops-automated-setups/linux/modules/setup-cronie.sh
SETUP_DOCKER=~/devops-automated-setups/linux/modules/setup-docker.sh
SETUP_GIT=~/devops-automated-setups/linux/modules/setup-git.sh
SETUP_YARN=~/devops-automated-setups/linux/modules/setup-yarn.sh
SETUP_NODE=~/devops-automated-setups/linux/modules/setup-node.sh
SETUP_SYNCTHING=~/devops-automated-setups/linux/modules/setup-syncthing.sh
SETUP_RUST=~/devops-automated-setups/linux/modules/setup-rust.sh
SETUP_PYTHON=~/devops-automated-setups/linux/modules/setup-python.sh

# Variables
REQUIRED_PACKAGES="ca-certificates \
  curl \
  gnupg"
MAIN_PACKAGES="nodejs \
  yarn \
  arduino \
  make \
  automake \
  gcc \
  gcc-c++ \
  kernel-devel \
  htop \
  fzf"
# System specific Packages
ARCH_PACKAGES="lsb-release"
REDHAT_PACKAGES="redhat-lsb-core \
  dnf-plugins-core"
DEBIAN_PACKAGES="lsb-core"

# Setup
cd ~

$PACKAGE_INSTALLER $REQUIRED_PACKAGES
$PACKAGE_INSTALLER $MAIN_PACKAGES
$SETUP_GIT
$SETUP_NODE
$SETUP_YARN
$SETUP_VIM
$SETUP_TMUX
$SETUP_POSTGRESQL
$SETUP_CRONIE
$SETUP_DOCKER
$SETUP_SYNCTHING
$SETUP_RUST
$SETUP_PYTHON

if [ $PACKAGE_MANAGER == "pacman" ]
then
  $PACKAGE_INSTALLER $ARCH_PACKAGES
elif [ $PACKAGE_MANAGER == "dnf" ]
then
  $PACKAGE_INSTALLER $REDHAT_PACKAGES
elif [ $PACKAGE_MANAGER == "apt" ]
then
  $PACKAGE_INSTALLER $DEBIAN_PACKAGES
fi

# Install packages dependant on the main packages
npm install -g typescript
pip install grip
